import React from "react";
import ClientsListItem from '../ClientsListItem/ClientsListItem';


const ClientsList = ({clientsList}) => {
  
    const listItems = clientsList.map((client) => {
        return <ClientsListItem key={client.id} client={client}/>
    })


  return (
    <table className="ClientsList" data-testid="ClientsList">
      
      <tbody>
      <h3>Clients</h3>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Created date</th>
            <th>Company</th>
          </tr>
          {listItems}
      </tbody>
    </table>
  );
};

export default ClientsList;