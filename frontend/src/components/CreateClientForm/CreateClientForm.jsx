import React from "react";
import { useForm } from 'react-hook-form';
import axios from 'axios';

const CreateClientForm = ({onClientCreated }) => {
    const { register, handleSubmit, reset } = useForm();
    
    const onSubmit = data => {
        axios.post('http://localhost:3001/clients/add', {data: data})
        .then(response => {
            console.log('new client ', response.data);
            const newClient = response.data;
            reset({name: "", email: "", company: ""})
            onClientCreated(newClient);
        })
    };
    
    return (
        <form onSubmit={handleSubmit(onSubmit)} className="CreateClientForm" data-testid="CreateClientForm">
            <label htmlFor="label" id="name">Name</label>
            <input className="input" defaultValue="" {...register("name")}/><br/>
            <label htmlFor="label" id="email">Email</label>
            <input className="input" defaultValue="" {...register("email")}/><br/>
            <label htmlFor="label" id="company">Company</label>
            <input className="input" defaultValue="" {...register("company")}/><br/>
            <input type="submit" />
        </form>
        );
};

export default CreateClientForm;