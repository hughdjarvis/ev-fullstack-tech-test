import React from 'react';
import renderer from 'react-test-renderer';
import CreateClientForm from './CreateClientForm';

it('renders correctly' , () => {
    const tree = renderer.create(<CreateClientForm onClientCreated={() => {}}/>).toJSON();
    expect(tree).toMatchSnapshot();
});