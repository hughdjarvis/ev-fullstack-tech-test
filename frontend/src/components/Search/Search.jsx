import React, { useState, useEffect, useCallback } from "react";
import ClientsList from "../ClientsList/ClientsList";


const Search = ({clientsList}) => {
    const [searchResults, setSearchResults ] = useState(clientsList);
    const [search, setSearch ] = useState("");

    const filterClients = useCallback(() => {
        console.log('clients list ', clientsList);
        console.log('search ', search);
        const results = clientsList.filter((client) => {
            client.name.toLowerCase().includes(search.toLowerCase())
        })
        console.log('search results',  results);
        setSearchResults(results);
    }, []);

    useEffect(() => {
        filterClients();
    }, [filterClients, search]);

  return (
    <div className="Search" data-testid="Search">
        <label htmlFor="">Search client name</label>
        <input type="text" onChange={event => setSearch(event.target.value)} placeholder="Search..." className="input" defaultValue=""></input> 
        <ClientsList clientsList={searchResults}/>
    </div>   
  );
};

export default Search;