import React from 'react';
import renderer from 'react-test-renderer';
import Search from './Search';

it('renders correctly' , () => {
    let dummyClientsList = [
        { 
          id: 1,
          name: "Madeup Person",
          email: "madeupperson@madeupaddress.com",
          createdDate: "27-11-2021",
          company: "Madeup Company"
        },
        {
          id: 2,
          name: "Fake Person",
          email: "fakeperson@madeupaddress.com",
          createdDate: "27-11-2021",
          company: "Fake Company"},
        {
          id: 3,
          name: "Bogus Person",
          email: "bogusperson@bogusaddress.com",
          createdDate: "27-11-2021",
          company: "Bogus Company"}
      ]
    const tree = renderer.create(<Search clientsList={dummyClientsList}/>).toJSON();
    expect(tree).toMatchSnapshot();
});