import React from 'react';
import renderer from 'react-test-renderer';
import ClientsListItem from './ClientsListItem';

it('renders correctly when passed a client', () => {
    const client = {
        id: 1,
        name: "Hugh Test",
        email: "hughtest@test.com",
        company: "Test Ltd"
    }
    const tree = renderer.create(<ClientsListItem client={client}/>).toJSON();
    expect(tree).toMatchSnapshot();
}); 