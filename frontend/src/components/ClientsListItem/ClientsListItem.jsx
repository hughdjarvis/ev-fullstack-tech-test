import React from "react";


const ClientsListItem = ({client}) => {

  return (
    <tr className="ClientsListItem" data-testid="ClientsListItem">
      <td>{client.name}</td>
      <td>{client.email}</td>
      <td>{client.createdDate}</td>
      <td>{client.company}</td>
    </tr>
  );
};

export default ClientsListItem;