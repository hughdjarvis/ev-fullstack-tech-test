import React, { useState, useCallback, useEffect } from "react";
import axios from 'axios';
import CreateClientForm from "./components/CreateClientForm/CreateClientForm";
import ClientsList from "./components/ClientsList/ClientsList";

const App = () => {
  const [clientsList, setClientsList ] = useState();
  const [showForm, setShowForm ] = useState(false);

  const onClientCreated = (client) => {
    // TODO clear the form onsubmit, not here
    setShowForm(!showForm);
    const updatedClientsList = clientsList.concat(client)
    setClientsList(updatedClientsList);
  }

  // fetch list of clients and use that to set the clientsList state
  const getClients = useCallback (async () => {
    axios.get('http://localhost:3001/clients')
    .then(response => setClientsList(response.data))
  }, []);

  useEffect(() => {
    getClients();
  }, [getClients])

  return (
    <div className="App" data-testid="App">
      <h1>EVPro Full-stack Test</h1>
      {/* TODO swap ClientsList component for Search component, then  
      let Search component render the ClientsList*/}
      {typeof clientsList !== "undefined" ? <ClientsList clientsList={clientsList}/> : null}
      { !showForm ? <button onClick={() => {setShowForm(!showForm) }}>Add client</button> : 
       <button onClick={() => {setShowForm(!showForm) }}>Cancel</button>}
       { showForm ? <CreateClientForm onClientCreated={onClientCreated} /> : null}
    </div>
  );
};

export default App;
