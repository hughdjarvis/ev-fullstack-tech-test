const express = require("express");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors({
  origin: "http://localhost:3000"
}));

// dummy data to return
let dummyClientsList = [
  { 
    id: 1,
    name: "Madeup Person",
    email: "madeupperson@madeupaddress.com",
    createdDate: "27-11-2021",
    company: "Madeup Company"
  },
  {
    id: 2,
    name: "Fake Person",
    email: "fakeperson@madeupaddress.com",
    createdDate: "27-11-2021",
    company: "Fake Company"},
  {
    id: 3,
    name: "Bogus Person",
    email: "bogusperson@bogusaddress.com",
    createdDate: "27-11-2021",
    company: "Bogus Company"}
]

// functions to mimic server/db actions for creating new clients
function getTodaysDate () {
  let date = new Date();
  date = date.toLocaleDateString('en-GB').toString();
  return date.substring(0,10);
}

function generateId () {
  return Math.floor(Math.random() * 100);
}

app.get('/clients', (req, res) => {
  // TODO get real data from db
  res.send(dummyClientsList)
});

app.post('/clients/add', (req, res) => {
  console.log(req.body);
  const newClient = {
    id: generateId(),
    name: req.body.data.name,
    email: req.body.data.email,
    createdDate: getTodaysDate(),
    company: req.body.data.company
  }
  res.send(newClient);
});

app.listen(3001, () => {
  console.log("Server started on port 3001");
});
